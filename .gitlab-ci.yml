stages:
  - build

# All jobs will use this Docker image:
image: ubuntu:20.04

variables:
  # As the libopencm3 library is a git submodule, enable recursive cloning:
  GIT_SUBMODULE_STRATEGY: recursive
  # Set the clone depth large enough to reach the most recent version tag so that the output artifacts are nicely/more clearly named:
  GIT_DEPTH: 500
  # Prevent the installer for pkg-config (& perhaps others) from prompting the user:
  DEBIAN_FRONTEND: "noninteractive"

# All jobs should run these commands first:
before_script:
  # Set up a handy variable:
  - COMMIT_SHORT_SHA=`expr substr $CI_COMMIT_SHA 1 8`

  # Prepare to install required tools/packages:
  - apt-get -y update

  # The following packages include the 'make' & 'cmake' tools:
  - apt-get install -y build-essential
  - apt-get install -y cmake

  # The following packages are required by one or more of the makefiles:
  - apt-get install -y python3

  # The following tool allows this script to collect files from the internet:
  - apt-get install -y curl

  # The following tools are required so that this script can rename the output bin file to include the git-commit description:
  - apt-get install -y git
  - apt-get install -y rename

  # Get a copy of the ARM gcc tools:
  - mkdir ${CI_PROJECT_DIR}/downloads
  - cd ${CI_PROJECT_DIR}/downloads
  - curl -L --output "gcc-arm-none-eabi.tar.bz2" https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2
  - mkdir ${CI_PROJECT_DIR}/opt
  - cd ${CI_PROJECT_DIR}/opt
  - tar xjf ${CI_PROJECT_DIR}/downloads/gcc-arm-none-eabi.tar.bz2
  - mv gcc-arm-none-eabi-9-2020-q2-update gcc-arm
  - export PATH="${CI_PROJECT_DIR}/opt/gcc-arm/bin:$PATH"
  - cd ${CI_PROJECT_DIR}

build_job:
  stage: build
  script:
    # Check that gcc was installed correctly:
    - arm-none-eabi-gcc --version
    # Compile libopencm3 & lib which will be used by the main firmware:
    - make -C libopencm3 lib
    # Build the main firmware:
    - cmake .
    - make

    # Rename each the target device's binary file to incorporate the version/commit information in the filename:
    - ARTIFACTS_VERSION=$(git describe --always --dirty --tags)
    - cd ${CI_PROJECT_DIR}/src
    - rename "s/.bin/-${ARTIFACTS_VERSION}.bin/g" *
    - mv ./*.bin ${CI_PROJECT_DIR}
    - cd ${CI_PROJECT_DIR}

  artifacts:
    paths:
      # Save the output binary file (*.bin) for the target device as a pipeline artifact:
      - ./*.bin

  only:
    # Only trigger/run the GitLab CI/CD pipeline when a commit occurs to the release/release-candidate*/develop branch or when triggered from the web GUI.
    - release
    - /^release-candidate.*$/
    - develop
    - web
